import { InjectedConnector } from '@web3-react/injected-connector';
import Web3 from 'web3/dist/web3.min';

const connector = new InjectedConnector({
    supportedChainIds: [
        1, 3, 4, 5, 42, 5494202210
    ]
})

const getLibrary = (provider) => {
    return new Web3(provider)
}

export {
    connector,
    getLibrary
}